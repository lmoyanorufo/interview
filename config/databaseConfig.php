<?php

$allVarsDefined = getenv('HOST_DB') &&
                  getenv('DB_NAME') &&
                  getenv('USER_DB') &&
                  getenv('PASS_DB');

if ($allVarsDefined){
    define('HOST_DB',getenv('HOST_DB'));
    define('DB_NAME',getenv('DB_NAME'));
    define('USER_DB',getenv('USER_DB'));
    define('PASS_DB',getenv('PASS_DB'));
    
    echo "estan las variables definidas";
}
else{
    $configEnv = json_decode(file_get_contents(__DIR__.'/../config/env_vars_dev.json'),true);
    $configDatabase = $configEnv['database'];
    
    define('HOST_DB',$configDatabase['host']);
    define('DB_NAME',$configDatabase['dbname']);
    define('USER_DB',$configDatabase['user']);
    define('PASS_DB',$configDatabase['password']);
}
