<?php

// configure your app for the production environment

$app['twig.path'] = array(__DIR__.'/../templates');
$app['twig.options'] = array('cache' => __DIR__.'/../var/cache/twig');

$app['validMimetypes'] = ['image/png', 'image/jpeg','image/gif'];
$app['maxImageSize'] = 2000000;
$app['maxWidth'] = 1920;
$app['maxHeight'] = 1080;
