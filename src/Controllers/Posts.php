<?php


namespace Controllers;

use Data\MapperBuilder;
use Operations\Posts as OperationPost;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Posts
{
    protected $app;
    protected $postsOperations;
    protected $counterViewsOperations;

    public function __construct($app)
    {
    	$this->app = $app;
        
        $operationsBuilder = \Operations\OperationsBuilder::getInstance($app);
        $this->postsOperations = $operationsBuilder->getOperations("Posts");
        $this->counterViewsOperations = $operationsBuilder->getOperations("CounterViews");
    }

    public function indexAction()
    {
        return $this->listPosts();
    }
    
    protected function listPosts($error = "")
    {
        $listPosts = $this->postsOperations->listAll();
        $posts = [];
        
        foreach($listPosts as $post){
            $posts[] = [
                'title' => $post->getTitle(),
                'image' => IMAGE_DIR."/".$post->getImage()
            ];
        }
        
        return $this->app['twig']->render('index.html.twig', [
                'posts' => $posts,
                'error' => $error
            ]
        );
    }
    

    public function newPostAction(Request $request)
    {        
        $title = (string)$request->request->get('imageTitle', "");
        $image = $request->files->get('pic', null);
        $messageResult = "";

        try{
            if (null === $image){
                throw new \Exception("No image found");
            }

            $imageValidator = new Validators\ImageValidator(
                $this->app['validMimetypes'], 
                $this->app['maxImageSize'],
                $this->app['maxWidth'], 
                $this->app['maxHeight']
            );

            if (!$imageValidator->validate($image)){
                throw new \Exception($imageValidator->getCause());
            }
            
            $fileName = uniqid("img");
            $image->move(IMAGE_DIR, $fileName);
            $this->postsOperations->createNewPost($title, $fileName);
            
        } catch (\Exception $ex) {
            $messageResult = $ex->getMessage();
        }

        return $this->listPosts($messageResult);
    }

    public function getStatsAction()
    {
    	return $this->app->json([
	        'views' => $this->counterViewsOperations->getCountPage("main"),
	        'posts' => $this->postsOperations->getNumPosts()
	    ]);
    }
    
    public function getExportAction()
    {
        $allPosts = $this->postsOperations->listAll();
        $tempFileName = tempnam('/tmp', 'allposts-'.  uniqid());
        $fp = fopen($tempFileName, 'w');
        
        $header = [
            'Title',
            'Path to photo'
        ];
      
        fputcsv($fp, $header);
        
        foreach($allPosts as $post){
            $postRow = [$post->getTitle(), IMAGE_DIR."/".$post->getImage()];
            fputcsv($fp, $postRow);
        }

        fclose($fp);
        $contentCSV = file_get_contents($tempFileName);
        unlink($tempFileName);
                
        return new Response(
            $contentCSV,
            200,
            array(
                'Content-Type'          => 'text/csv',
                'Content-Disposition'   => 'inline; filename="AllPosts.csv"'
            )
        );
    }
}
