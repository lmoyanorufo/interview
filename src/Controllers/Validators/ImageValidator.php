<?php

namespace Controllers\Validators;

class ImageValidator{
    
    protected $validFormats;
    protected $maxSize;
    protected $maxWidth;
    protected $maxHeight;
    protected $reasonFail = "";
    
    public function __construct($validFormats, $maxSize, $maxWidth, $maxHeight) 
    {
        $this->validFormats = $validFormats;
        $this->maxSize = $maxSize;
        $this->maxWidth = $maxWidth;
        $this->maxHeight = $maxHeight;
    }
    
    public function validate($image)
    {     
        $size = filesize($image);
        
        if ($size > $this->maxSize){
            $this->reasonFail = "Max size must be: ".$this->maxSize;
            return false;
        }

        $mimeTypeGetter = finfo_open(FILEINFO_MIME_TYPE);
        $mimeType = finfo_file($mimeTypeGetter, $image);
        if (!in_array($mimeType, $this->validFormats)) {
            $this->reasonFail = "Invalid file. Must be ".  implode(",", $this->validFormats);
            return false;
        }
        
        $imagesize = getimagesize($image);
        
        $heightGreaterThanMax = $imagesize[1] > $this->maxHeight;
        $widthGreaterThanMax = $imagesize[0] > $this->maxWidth;
        
        if ($heightGreaterThanMax || $widthGreaterThanMax){
            $this->reasonFail = "Image is greather than ".$widthGreaterThanMax."x".$heightGreaterThanMax;
            return false;
        }
        
        return true;
    }
    
    public function getCause()
    {
        return $this->reasonFail;
    }
}
