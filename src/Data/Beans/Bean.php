<?php

namespace Data\Beans;

class Bean
{
    public function fillFromRow($row)
    {
        foreach($row as $key => $value){
            $this->$key = $value;
        }
    }
}