<?php

namespace Data\Mappers;

class CounterViews {

    protected $db;

    public function __construct($db) 
    {
        $this->db = $db;
    }
    
    public function increasePage($page) 
    {
        $sql = "INSERT INTO `counter_views` (`route`, `counter`) "
              ."VALUES (:page, '1') "
              ."ON DUPLICATE KEY UPDATE counter = counter + 1";

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam("page", $page);
        $stmt->execute();
    }
    
    public function getCountPage($page) 
    {
        try{
            $sql = "SELECT counter from counter_views where route = :page";

            $stmt = $this->db->prepare($sql);
            $stmt->bindParam("page", $page);
            $stmt->execute();
            $result = $stmt->fetch();
            
            if ($result){
                return $result['counter'];
            }
            else{
                return 0;
            }
        } catch (\Exception $ex) {
            return 0;
        }
    }
}
