<?php

namespace Data\Mappers;

use Data\Beans\Post;

class Posts {

    protected $db;

    public function __construct($db) 
    {
        $this->db = $db;
    }

    public function listAll($order = "") 
    {
        $sql = "SELECT * from posts ORDER by ts ".$order;
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll();

        $return = [];
        foreach ($results as $result) {

            $post = new Post();
            $post->fillFromRow($result);
            $return[] = $post;
        }

        return $return;
    }

    public function createNewPost($title, $image) 
    {
        $sql = "INSERT INTO `posts` (`title`, `image`) VALUES (:title, :image)";

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam("title", $title);
        $stmt->bindParam("image", $image);
        $stmt->execute();
    }

    public function getNumPosts() 
    {
        $sql = "SELECT count(*) as numPost from posts";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();

        return $result['numPost'];
    }

}
