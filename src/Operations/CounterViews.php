<?php

namespace Operations;


class CounterViews 
{

    protected $counterViewsMapper;

    public function __construct($counterViewsMapper) 
    {
        $this->counterViewsMapper = $counterViewsMapper;
    }

    public function increasePage($page)
    {
        return $this->counterViewsMapper->increasePage($page);
    }

    public function getCountPage($page) 
    {
        return $this->counterViewsMapper->getCountPage($page);
    }
}