<?php 

namespace Operations;
	
class OperationsBuilder
{	
	private static $instance = null;
	protected $app;
    protected $mapperBuilder;

	public function __construct($app)
	{	
		$this->app = $app;
        $this->mapperBuilder = \Data\MapperBuilder::getInstance($app);
	}

	public function getOperations($nameClass)
	{   
        switch($nameClass){
            case 'Posts':
                return new Posts($this->mapperBuilder->getMapper('Posts'));
            case 'CounterViews':
                return new CounterViews($this->mapperBuilder->getMapper('CounterViews'));
            default:
                throw new \Exception("OPERATIONS_DOESNT_EXISTS");
        }
	}

	public static function getInstance($app)
	{
		if (null === self::$instance){
			self::$instance = new OperationsBuilder($app);
		}

		return self::$instance;
	}
}