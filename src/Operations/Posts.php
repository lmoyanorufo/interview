<?php

namespace Operations;


class Posts 
{

    protected $postMapper;

    public function __construct($postMapper) 
    {
        $this->postMapper = $postMapper;
    }

    public function listAll() 
    {
        return $this->postMapper->listAll("desc");
    }

    public function createNewPost($title, $imagePath) 
    {
        $this->postMapper->createNewPost($title, $imagePath);
    }
    
    public function getNumPosts() 
    {
        return $this->postMapper->getNumPosts();
    }
}
