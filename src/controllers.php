<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Controllers\Posts;


$app['index.controller'] = function ($app) {
    return new Controllers\Posts($app);
};

$app->get('/', 'index.controller:indexAction')->bind('main');
$app->post('/', 'index.controller:newPostAction')->bind('newPost');
$app->get('/stats', 'index.controller:getStatsAction' )->bind('stats');
$app->get('/export', 'index.controller:getExportAction' )->bind('export');

$app->before(function (Request $request) use ($app) {
   
    $route = $request->get('_route');
    
    if ($route === 'main'){
        $operationsBuilder = \Operations\OperationsBuilder::getInstance($app);
        $counterViews = $operationsBuilder->getOperations("CounterViews");
        $counterViews->increasePage($route);
    }
});



$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});
